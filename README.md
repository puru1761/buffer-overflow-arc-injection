# DevTodo Exploit

## Application Used
The application chosen to exploit is called [DevTodo v1.20][df1]. It is a simple command line utility that allows you to maintain todo list. The original source code for it can be found [here.][df2]

## Prerequisites to import into eclipse
The makefile provided with the source code for DevTodo generates a config.h file that must be generated and stored with the rest of the source code. To get this make file, run the following commands in the directory where the extracted contents of the [source code][df2] lie.

```sh
$ ./configure
$ make
```

## Importing into Eclipse
Begin by creating a new C++ project and inserting the two source code folders (src and util) from the source-zip file.
Place the generated config.h file into the src folder as well. Change the path of header files in all files to their respective paths, for example, #include<CommandArgs.h> should be #include<../util/CommandArgs.h>.

This step needs to be performed on all files in the src folder to make sure all the references are found.

Build settings are modified as mentioned in the [Eclipse notes][df3] to allow an executable stack, build the file as 32 bit amongst other things.

In the build settings, two library flags were also mentioned since DevTodo requires them to run. These two libraries are readline and ncurses.

The program in [src/todoterm.cc][df8] makes use of environment variables but had not imported the required header file for it. We also added the header stdlib.h

Now the project can be compiled and run.

## Introducing Vulnerabilities

We introduced the vulnerability in the main file of the program and renamed it to [exp.cc][df9]This was done so that we could write our exploit program in the main.cc file to exploit the vulnerability introduced in the DevTodo utility. We wrote our driver program with the main function in [exploitable.cc][df10]. 

Now the file exploitable.cc is a driver program that contains both our exploits for Format String Vulnerability and Buffer Overflow.

exp.cc contains the code to make the program vulnerable to buffer overflow.
Parallel to this, exploitable.cc contains the exploitation code for the above mentioned buffer overflow vulnerability.


By default, we have commented out the code in exp.cc and exploitable.cc that run the buffer overflow vulnerability and left the code active which exploits format string vulnerability.

## Exploit Driver file

The newly generated main file (exploitable.cc) now takes two arguments
- First, shellcode address, i.e. address of the shell code in the system
- Second, return pointer address, i.e. the address at which the function's return address is written

This driver file internally capitalizes on the "add" command of the DevTodo utility. The original utility required two arguments as well in the form of a flag and the data along with it. The driver file uses the "-a" or "add" flag and as data to be inserted into the todo list, sends in a maliciously constructed tainted buffer that exploits the required vulnerability.

Unexploited program example in add mode
```sh
$ todo -a "Submit SVA homework 3"
```

However since the main file is now our exploit driver program, we pass in the two addresses (shellcode and return pointer). Exploited program running via our exploit driver (exploitable.cc):
```sh
$ todo 0xFFFFABAB 0xFFFFEFEF
```
Our exploit driver program internally calls the original todo utility with the malicious tainted buffer instead of legit data.

## Team Members
These exploits were developed by
  - Purushottam Kulkarni (pkulkar6)
  - Ritvik Sachdev (rsachde4)

   [df1]: <http://swapoff.org/devtodo1.html>
   [df2]: <http://swapoff.org/files/devtodo/devtodo-0.1.20.tar.gz>
   [df3]: <http://192.168.200.150/SVA/KnowledgeManagement/wikis/EclipseNotes>
   [df4]: <http://sva-gitlab.mssi-lab.isi.jhu.edu/pkulkar6/HW3-format-string-exploit/blob/master/devtodo/src/exp.cc#L34-41>
   [df5]: <http://sva-gitlab.mssi-lab.isi.jhu.edu/pkulkar6/HW3-format-string-exploit/blob/master/devtodo/src/exploitable.cc#L50-72>
   [df6]: <http://sva-gitlab.mssi-lab.isi.jhu.edu/pkulkar6/HW3-format-string-exploit/blob/master/devtodo/src/exp.cc#L43-59>
   [df7]: <http://sva-gitlab.mssi-lab.isi.jhu.edu/pkulkar6/HW3-format-string-exploit/blob/master/devtodo/src/exploitable.cc#L74-220>
   [df8]: <http://sva-gitlab.mssi-lab.isi.jhu.edu/pkulkar6/HW3-format-string-exploit/blob/master/devtodo/src/todoterm.cc>
   [df9]: <http://sva-gitlab.mssi-lab.isi.jhu.edu/pkulkar6/HW3-format-string-exploit/blob/master/devtodo/src/exp.cc>
   [df10]: <http://sva-gitlab.mssi-lab.isi.jhu.edu/pkulkar6/HW3-format-string-exploit/blob/master/devtodo/src/exploitable.cc>

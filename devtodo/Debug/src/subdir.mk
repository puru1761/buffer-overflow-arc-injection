################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../src/Loaders.cc \
../src/Todo.cc \
../src/TodoDB.cc \
../src/exp.cc \
../src/exploitable.cc \
../src/support.cc \
../src/todoterm.cc 

CC_DEPS += \
./src/Loaders.d \
./src/Todo.d \
./src/TodoDB.d \
./src/exp.d \
./src/exploitable.d \
./src/support.d \
./src/todoterm.d 

OBJS += \
./src/Loaders.o \
./src/Todo.o \
./src/TodoDB.o \
./src/exp.o \
./src/exploitable.o \
./src/support.o \
./src/todoterm.o 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -m32 -Wno-format-security -fno-stack-protector -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


